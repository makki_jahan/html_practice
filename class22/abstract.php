<?php
abstract class User
{
    protected $username;
    abstract public function stateYourRole();

    public function setname($name)
    {
        $this->username = $name;
    }

    public function getname()
    {
        return $this->username;
    }
}

class Admin extends  User
{

    public function stateYourRole()
    {
        return "Admin";
    }
}


class Viewer extends User
{
    public function stateYourRole()
    {
        strtolower(__CLASS__);
    }
}
