<?php

class Color
{

    public $name = 'Parent';
    public function colorname()
    {
        echo "Green";
    }
}

class Color2 extends Color
{

    public $name = 'child';
    public function  colorname()
    {
        echo "Blue";
    }
}

$ColorObject2 = new Color2;
echo $ColorObject2->name;
