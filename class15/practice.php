 
<?php

$students = [
    'cse' => [
        ['name' => 'Himel', 'email' => 'himel@email.com', 'result' => '4'],
        ['name' => 'Zarin', 'email' => 'zarin@email.com', 'result' => '4'],
    ],
    'bba' => [
        ['name' => 'Tamim', 'email' => 'tamim@email.com', 'result' => '4'],
        ['name' => 'Kafi', 'email' => 'kafi@email.com', 'result' => '3.9'],
    ],
];

foreach ($students as $key => $department) {
    echo 'Department: ' . $key . '
Students:
';
    $i = 1;
    foreach ($department as $student) {
        echo $i . '. Name: ' . $student['name'] . ', Email: ' . $student['email'] . ', Result: ' . $student['result'] . '
';
        ++$i;
    }
    echo '
';
}
