<?php


class Student
{
    public $conn;

    public function __construct()
    {
        session_start();
        try {


            $this->conn = new PDO("mysql:host=localhost;dbname=ecommerce", 'root', 'root');
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $exception) {

            $_SESSION['error'] = $exception->getMessage();
            header('location:../students/create.php');
            //echo $exception->getMessage(); 


        }
    }





    public function store($data)
    {
        try {
            $fname = $data['fname'];
            $lname = $data['lname'];
            $address = $data['address'];
            $mobile = $data['mobile'];
            $query = "insert into students_db(fname,lname,address,mobile) values(:students_fname,:students_lname,:students_adress,:students_mobile)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'students_fname' => $fname,
                'students_lname' => $lname,
                'students_adress' =>  $address,
                'students_mobile' =>   $mobile,
            ]);

            $_SESSION['message'] = "successfully created!";
            header('location:../students/index.php');
        } catch (PDOException $exception) {
            echo $exception->getMessage();
        }
    }


    public function index()
    {

        $query =   "select * from students_db";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }


    public function show($id)
    {

        $query =   "select * from students_db where id =:students_id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([
            'students_id' => $id
        ]);

        return $stmt->fetch();
    }



    public function edit($id)
    {

        $query =   "select * from students_db where id =:students_id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([
            'students_id' => $id
        ]);

        return $stmt->fetch();
    }


    public function update($data)
    {
        try {
            $id = $data['id'];
            $fname = $data['fname'];
            $lname = $data['lname'];
            $address = $data['address'];
            $mobile = $data['mobile'];

            $query = "update students_db set  fname=:students_fname, lname=:students_lname,
            address=:students_address, mobile=:students_mobile where id=:students_id";

            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'students_id' => $id,
                'students_fname' => $fname,
                'students_lname' => $lname,
                'students_address' =>  $address,
                'students_mobile' =>   $mobile
            ]);

            $_SESSION['message'] = "successfully Updated!";
            header('location:../students/index.php');
        } catch (PDOException $exception) {
            echo $exception->getMessage();
        }
    }



    public function destroy($id)
    {

        try {
            $query =   "delete  from students_db where id =:students_id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'students_id' => $id,
            ]);

            $_SESSION['message'] = "successfully deleted!";
            header('location: ../view/students/index.php');
        } catch (PDOException $exception) {
            $_SESSION['error'] = $exception->getMessage();
            header('location:../view/students/index.php');
        }
    }
}
