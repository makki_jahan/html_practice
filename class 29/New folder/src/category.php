<?php


class Student
{
    public $conn;

    public function __construct()
    {
        session_start();
        try {


            $this->conn = new PDO("mysql:host=localhost;dbname=new_ecomm", 'root', 'root');
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $exception) {

            $_SESSION['error'] = $exception->getMessage();
            header('location:../students/create.php');
            //echo $exception->getMessage(); 


        }
    }





    public function store($data)
    {
        try {
            $title = $data['title'];
            $description = $data['description'];

            $query = "insert into categories_table(title,description) values(:categories_title,:categories_description)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'categories_title' => $title,
                'categories_description' => $description,

            ]);

            $_SESSION['message'] = "successfully created!";
            header('location:../students/index.php');
        } catch (PDOException $exception) {
            echo $exception->getMessage();
        }
    }


    public function index()
    {

        $query =   "select * from categories_table";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }


    public function show($id)
    {

        $query =   "select * from categories_table where id =:categories_id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([
            'categories_id' => $id
        ]);

        return $stmt->fetch();
    }



    public function edit($id)
    {

        $query =   "select * from categories_table where id =:categories_id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([
            'categories_id' => $id
        ]);

        return $stmt->fetch();
    }


    public function update($data)
    {
        try {
            $id = $data['id'];
            $title = $data['title'];

            $query = "update students_db set  title=:categories_title, description=:categories_description;
             
            $stmt = $this->conn->prepare($query)
            $stmt->execute([
                'categories_title' => $title,
                'categories_description' => $description,

            ]);

            $_SESSION['message'] = "successfully Updated!";
            header('location:../students/index.php');
        } catch (PDOException $exception) {
            echo $exception->getMessage();
        }
    }



    public function destroy($id)
    {

        try {
            $query =   "delete  from students_db where id =:students_id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'students_id' => $id,
            ]);

            $_SESSION['message'] = "successfully deleted!";
            header('location: ../view/students/index.php');
        } catch (PDOException $exception) {
            $_SESSION['error'] = $exception->getMessage();
            header('location:../view/students/index.php');
        }
    }
}
