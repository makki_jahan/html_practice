<?php



class Calculator
{
    public $number1;
    public $number2;


    public function __construct($num1, $num2)
    {
        $this->number1 = $num1;
        $this->number2 = $num2;
    }


    public function Add()
    {
        return  $result = $this->number1 + $this->number2;
    }


    public function Sub()
    {
        return  $result = $this->number1 - $this->number2;
    }


    public function Mul()
    {
        return  $result = $this->number1 * $this->number2;
    }

    public function Div()
    {
        return  $result = $this->number1 / $this->number2;
    }
}

$CalculatorObject = new Calculator($_POST['number1'], $_POST['number2']);


if (isset($_POST['Add'])) {
    echo $CalculatorObject->Add() . " ";
}

if (isset($_POST['Sub'])) {
    echo $CalculatorObject->Sub() . " ";
}

if (isset($_POST['Mul'])) {
    echo $CalculatorObject->Mul() . " ";
}

if (isset($_POST['Div'])) {
    echo $CalculatorObject->Div() . " ";
}
