<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    if (isset($_SESSION['message'])) {
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }
    ?>
    <form action="processlogin.php" method="POST">
        <label for="fname"> Name:</label><br>
        <input type="text" id="fname" name="fname"><br><br>
        <label for="password">password:</label><br>
        <input type="password" id="password" name="password"><br><br>
        <button type="submit">LogIn</button>
    </form>
</body>

</html>