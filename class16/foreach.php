<?php

$students = [
    'cse' => [
        ['name' => 'Himel', 'email' => 'himel@email.com', 'result' => '4'],
        ['name' => 'Zarin', 'email' => 'zarin@email.com', 'result' => '4'],
    ],
    'bba' => [
        ['name' => 'Tamim', 'email' => 'tamim@email.com', 'result' => '4'],
        ['name' => 'Kafi', 'email' => 'kafi@email.com', 'result' => '3.9'],
    ],
];

foreach ($students as $key => $value) {

    if (is_array($value) == 1) {
        $i = 1;
        foreach ($value as   $value2) {
            echo $i . '<br>';
            $i++;
            if (is_array($value2) == 1) {

                foreach ($value2 as  $k =>  $value3)

                    echo    $k . ":" . $value3 . '<br>';
            }
        }
    }
}
