<?php

class Student
{
    public $conn;

    public function __construct()
    {
        session_start();
        try {


            $this->conn = new PDO("mysql:host=localhost;dbname=makki", 'root', 'root');
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $exception) {

            $_SESSION['error'] = $exception->getMessage();
            header('location:../views/users/register.php');
            //echo $exception->getMessage(); 


        }
    }


    public function store($data)
    {
        try {
            $fname = $data['fname'];
            $lname = $data['lname'];
            $address = $data['address'];
            $mobile = $data['mobile'];
            $email = $data['email'];
            $query = "insert into user_table(fname,lname,address,mobile,email) values(:students_fname,:students_lname,:students_adress,:students_mobile, :'students_email')";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'students_fname' => $fname,
                'students_lname' => $lname,
                'students_adress' => $address,
                'students_mobile' => $mobile,
                'students_email' => $email,
            ]);

            $_SESSION['message'] = "successfully created!";
            header('location:../users/views/registration.php');
        } catch (PDOException $exception) {
            echo $exception->getMessage();
        }
    }


    public function index()
    {

        $query =   "select * from students_db";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }
}
