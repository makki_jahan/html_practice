<?php
include_once('Displayer.php');
class Rectangle extends Shape
{
    use Displayer;
    public $length;
    public $width;


    public function __constructor($l, $w)
    {
        $this->length = $l;
        $this->width = $w;
    }

    public function calarea()
    {

        return $this->length * $this->width;
    }

    // public function dispaly()
    // {
    // }
}
