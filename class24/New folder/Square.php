<?php
include_once('Displayer.php');

class Square extends Shape
{
    use Displayer;

    public $length;
    public $width;


    public function __constructor($l, $w)
    {
        $this->length = $l;
        $this->width = $w;
    }

    public function calarea()
    {

        return $this->length * $this->length;
    }

    // public function dispaly()
    // {
    // }
}
