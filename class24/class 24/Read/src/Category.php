<?php


class Category
{
    public $conn;

    public function __construct()
    {
        session_start();
        try {


            $this->conn = new PDO("mysql:host=localhost;dbname=ecommerce1", 'root', 'root');
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $exception) {

            $_SESSION['error'] = $exception->getMessage();
            header('location:../Categories/create.php');
            //echo $exception->getMessage(); 


        }
    }


    public function index()
    {

        $query =   "select * from categories";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }


    public function store($data)
    {
        try {
            $title = $data['title'];
            $description = $data['Description'];
            $query = "insert into categories(title,Description) values(:category_title,:category_description)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'category_title' => $title,
                'category_description' => $description,
            ]);

            $_SESSION['message'] = "successfully created";
            header('location:../Categories/index.php');
        } catch (PDOException $exception) {
            echo $exception->getMessage();
        }
    }
}
