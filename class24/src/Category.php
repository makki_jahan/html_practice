<?php
class Category
{
    public $conn;
    public function __construct()
    {

        try {
            $this->conn = new PDO("mysql:host=localhost;dbname=ecommerce", 'root', 'root');
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $exception) {
        }
    }
    public function store($data)
    {
        try {
            $title = $data['title'];
            $query = "insert into categories(title) values(:category_title)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'category_title' => $title,
            ]);
        } catch (PDOException $exception) {
            echo "problem";
        }
    }
}
