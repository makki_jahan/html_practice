-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 12, 2022 at 05:47 AM
-- Server version: 5.7.33
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommrece`
--
CREATE DATABASE IF NOT EXISTS `ecommrece` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ecommrece`;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `cat_id`, `title`, `description`) VALUES
(1, 1, 'Makki', 'You will never know just how beautiful you are to me'),
(2, 2, 'Rownok', 'He is an achiever and a good student.'),
(3, 3, 'Hasan', ' Hasan lives in paris. He is prepairing for his IELTS exam!'),
(4, 3, 'Makki Jahan', 'Makki Jahan works hard in order to fulfill her dreams.');

-- --------------------------------------------------------

--
-- Table structure for table `students_db`
--

CREATE TABLE `students_db` (
  `ID` int(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `mobile` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students_db`
--

INSERT INTO `students_db` (`ID`, `fname`, `lname`, `address`, `mobile`) VALUES
(1, '   Makki', 'Chowdhury', 'Dhakaaa', 1784989491),
(2, 'Rownok', 'Chowdhury', 'Dinajpur', 144444),
(3, 'hasan', 'khan', 'uttara', 1756),
(4, ' Makki jahan', 'Chowdhury', 'Dhaka', 17849894);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students_db`
--
ALTER TABLE `students_db`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `fname` (`fname`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `students_db`
--
ALTER TABLE `students_db`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
