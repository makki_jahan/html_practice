-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 13, 2022 at 08:48 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `diescription` varchar(255) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`, `diescription`, `is_deleted`) VALUES
(3, 'web', 'retrtyyfugukhl', 1),
(4, 'software', 'ghjkbn.m/mmmmmmmmmmmmmmmmmm', 0),
(5, 'app', 'hgcjkblnl;', 0),
(6, 'Web Design', 'fsfsdskhdil dheileilljeo flfiljeojeo dljojdjeojd kjeojeo eoojeofe lefjofe lefejoefj', 0),
(7, 'logo design', 'this is logo design', 0);

-- --------------------------------------------------------

--
-- Table structure for table `employes`
--

CREATE TABLE `employes` (
  `id` int(11) NOT NULL,
  `roll` varchar(30) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employes`
--

INSERT INTO `employes` (`id`, `roll`, `name`, `phone`, `email`, `designation`, `department`) VALUES
(5, '8632', 'Mahmodul Hassan Shawon', '01763461865', 'shawonmahmodul12@gmail.com', 'afsg', 'dsafdbdf'),
(6, '642', 'Alomgir hossain', '01878844251', 'work251.info@gmail.com', 'janina', 'cse'),
(7, '965', 'jack jisan', '01746464646', 'jackjisan510@gmail.com', 'janina', 'eee'),
(8, '6632', 'Alomgir hossain1', '01756478132', 'abx442510@gmail.com', 'janina', 'cse');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `diescription` varchar(255) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `in_stok` tinyint(1) NOT NULL,
  `price` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `title`, `diescription`, `is_deleted`, `in_stok`, `price`, `discount`, `image`) VALUES
(6, 4, 'trtfykuhlijl', 'fygkhlj', 0, 1, 0, 0, ''),
(7, 5, 'this is app ', 'hfjgkjhljk;', 0, 0, 100, 20, ''),
(8, 3, 'this is product title', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus facilis odit blanditiis ab voluptatum veniam architecto quae eaque similique corporis?\r\n', 1, 1, 0, 0, ''),
(9, 3, 'ghvjb', 'sfdgfhgjj', 1, 1, 123, 56, NULL),
(10, 4, 'ghvjb', 'sfdgfhgjj', 1, 1, 120, 20, NULL),
(11, 3, 'this is title', 'ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd', 1, 1, 120, 2, NULL),
(12, 5, 'szdfghjkhlj', '', 0, 0, 84, 10, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `category_name` (`category_name`);

--
-- Indexes for table `employes`
--
ALTER TABLE `employes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `employes`
--
ALTER TABLE `employes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
