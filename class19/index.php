<?php

class Calculator
{

    public $Number1;
    public $Number2;

    public function __construct($num1, $num2)
    {
        $this->Number1 = $num1;
        $this->Number2 = $num2;
    }

    public function sum()
    {
        return $this->Number1 + $this->Number2;
    }

    public function sub()
    {
        return $this->Number1 - $this->Number2;
    }

    public function mul()
    {
        return $this->Number1 * $this->Number2;
    }

    public function div()
    {
        return $this->Number1 / $this->Number2;
    }
}

$Calculatorobject = new Calculator(10, 20);

echo $Calculatorobject->sum() . '<br>';
echo $Calculatorobject->sub() . '<br>';
echo $Calculatorobject->mul() . '<br>';
echo $Calculatorobject->div() . '<br>';
