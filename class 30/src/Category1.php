<?php
class Category
{
    public $con;
    public function __construct()
    {


        try {
            $this->con = new PDO("mysql:host=localhost;dbname=newproject", "root", "root");
        } catch (PDOException $e) {
            $_SESSION['message'] = $e->getMessage();
        }
    }

    public function addCategory($data)
    {

        session_start();

        $name = $data['name'];
        $description = $data['description'];

        try {
            $query = "insert into categories1(name, description) values(:category_name, :diescription)";
            $statement = $this->con->prepare($query);
            $statement->execute([
                ':category_name' => $name,
                ':diescription' => $description,

            ]);
            header("location: ../category1/addcategory.php");
        } catch (PDOException $e) {
            $_SESSION['massege'] = $e->getMessage();
            header("location: ../category1/addcategory.php");
        }
    }

    public function getCategoryList()
    {
        session_start();
        try {

            $query = "select *from categories1";
            $statement = $this->con->prepare($query);
            $statement->execute();
            $data = $statement->fetchAll();

            return $data;
        } catch (PDOException $e) {
            $_SESSION['massege'] = $e->getMessage();
            header('location: ../view/category1/index.php');
        }
    }

    public function show()
    {
        session_start();
        try {

            $query = "select *from categories1";
            $statement = $this->con->prepare($query);
            $statement->execute();
            $data = $statement->fetch();

            return $data;
        } catch (PDOException $e) {
            $_SESSION['massege'] = $e->getMessage();
            header('location: ../view/category1/index.php');
        }
    }
}
