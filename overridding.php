<?php

class Color
{

    public function __construct()
    {
        echo "Green";
    }
}

class Color2 extends Color
{

    public function __construct()
    {
        echo "Blue";
    }
}

$ColorObject = new Color;
